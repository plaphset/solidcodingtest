Feb 2021 SOLID Coding Test (SF Commands, Redis, PHPunit)
====================
### Coding challenge done in Feb 2021

### How to navigate the repository;
In `./SOLIDCodingTest/symfony/code/src/Command/` you will see two classes that are self explanatory:

- FeefoPrintReviewCommandINITIAL (this was pretty much everything that the initial src code contained)
- FeefoPrintReviewCommandREFACTORED

### The goals of the coding test were 
To take that initial Symfony command, which fetches and returns some customer review data from another web based RESTful API and

- refactor this code to "something more sensible"; so used SOLID principles, showcasing choices, approaches, coding style, while breaking the app into cleanly structured classes
- furthermore, to add server side caching for the requests (using Redis)
- to add new functionality through which to invalidate full app cache (another command).
- to implement unit testing (I used PHPUnit and some XML mocks and added broad test coverage)
- of course, while at it, added everything else that is sensible: proper input sanitization, improved data formatting, proper error handling, logging, wrote it following PSR standards, used a one-time CS fixer, etc.

### The reasons I personally liked this coding test are:
- **First and most important**: I think it showcases a relatively wide palette of challenges very similar to what I generally encountered in the real world
- It allows to show the thinking and the approach and architectural decisions I'm taking.
- It allowed me to show some nice layering approach for caching
- It allowed me to go for a nice and smart use of interfaces, which I'm quite proud of, so I want to draw your attention to that if you care to have a look
_____________

How to run locally
===================
Please note this code relies on another web RESTful API so if at any point, that API fails or is taken off the web, this will naturally not run anymore:
My wild guess is 2025 or later; it's early 2021 now :)

- clone the repo
- cd inside dir "SOLIDCodingTest" and all the following commands (should and) can be run:
- `docker-compose build`
- `docker-compose run --rm symfony composer install`
- Run the main feature with: `docker-compose run --rm symfony bin/console feefo:print-reviews 42687`
______
- Optionally of course, feel free to go inside redis container to inspect cached objects: 
  - `docker-compose exec redis /bin/bash`
  - `redis-cli`
  - `KEYS *` should show one key (what was cached just previously from running the print reviews command, here's another id: 171254)
  - `GET replace-key-here`, to check cached data containing the data model
______
- Run the tests with: `docker-compose run --rm symfony bin/phpunit` (first time should install dependencies)
- invalidate cache with: `docker-compose run --rm symfony bin/console app:cache:invalidate` (of course inspecting redis again with `KEYS *` shouldn't show anything anymore)
_____________

Optional: *If curious, here's the initial README (requirements) I was provided with:*
========

The aim of this code test is to get an idea about how you like to code, and if
it fits well with how we like to code!

Currently we have a console command which describes the reviews for an Amara
product:

```sh
docker-compose run --rm symfony bin/console feefo:print-reviews 42687
```

This should output something like:

"There are 2 reviews and the average is 100%"

Product id 42687 is for a "Vera Kettle", you can see it on the Amara site
here:

https://www.amara.com/products/vera-kettle

Just in case you'd like another id:

171254 is a "Prism Porcelain Mugs" (for your hot beverage!):

https://www.amara.com/products/prism-porcelain-mugs-set-of-4

Those reviews are coming from Feefo who collect customer reviews for Amara.

## The objectives


At the moment, every time we run the command it fetches the reviews from Feefo
and displays some text about them.

We have three changes we'd like to make:

1. Cache the reviews in Redis for a period of time, 24 hours, so we don't need
   to make lots of identical requests to Feefo. There is a `Predis\Client` service
   which you an use for this which is already set up.

2. Refactor the code into something more sensible, we want the Feefo reviews to
   be available to other, as-yet-unwritten parts of the system.

3. Provide a command to invalidate the cached reviews

The text output from our command should stay the same (well, unless the reviews
change!).

Finally, please list any issues you can see with the code which actually
retrieves the reviews.

### Code setup


You will need to run:

* `docker-compose build` to build the Docker container (which will mount the host's symfony/code to the container)
* `docker-compose run --rm symfony composer install` to prepare the composer dependencies.

Then you can:

* Run the command with: `docker-compose run --rm symfony bin/console feefo:print-reviews 42687`
* Run the tests with: `docker-compose run --rm symfony bin/phpunit`

