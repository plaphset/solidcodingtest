<?php

declare(strict_types=1);

namespace App\ApiUtil\Feefo;

use App\Cache\Feefo\FeefoRequesterInterface;
use App\DataTransformer\Feefo\ReviewsResponseTransformer;
use App\Logging\LoggerAwareTrait;
use App\Model\Feefo\ReviewsResponse;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerAwareInterface;
use Symfony\Component\HttpFoundation\Response;

class FeefoRequester implements LoggerAwareInterface, FeefoRequesterInterface
{
    use LoggerAwareTrait;

    public const FEEFO_XML_FEED_REVIEWS = '/feefo/xmlfeed.jsp';

    /**
     * @var ReviewsResponseTransformer
     */
    private $transformer;

    /**
     * @var ClientInterface
     */
    private $httpClient;

    public function __construct(ReviewsResponseTransformer $transformer, ClientInterface $httpClient)
    {
        $this->transformer = $transformer;
        $this->httpClient = $httpClient;
    }

    public function getReviewsByProductId(int $productId): ?ReviewsResponse
    {
        // for the purposes of this test, a simple array will have to do here, instead of any ENV configs
        $query = ['logon' => 'www.amara.co.uk', 'vendorref' => $productId, 'limit' => 1];

        try {
            $response = $this->httpClient->request('GET', static::FEEFO_XML_FEED_REVIEWS, [
                'query' => $query,
            ]);

            if (Response::HTTP_OK !== $response->getStatusCode()) {
                throw new \RuntimeException('Something went wrong fetching data from Feefo. Please see log context');
            }

            $responseContents = $response->getBody()->getContents();
        } catch (\Throwable $exception) {
            $this->getLogger()->error(
                $exception->getMessage(),
                [
                    'requestQuery' => $query,
                    'responseCode' => isset($response) ? $response->getStatusCode() : null,
                    'responseBody' => isset($response) ? $response->getBody() : null,
                    'responseHeaders' => isset($response) ? $response->getHeaders() : null,
                ]
            );
        }

        if (isset($responseContents)) {
            return $this->transformer->transform($responseContents);
        }

        return null;
    }
}
