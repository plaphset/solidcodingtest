<?php

declare(strict_types=1);

namespace App\Command;

use App\Cache\Invalidator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InvalidateAppCache extends Command
{
    protected static $defaultName = 'app:cache:invalidate';

    /**
     * @var Invalidator
     */
    private $invalidator;

    public function __construct(Invalidator $invalidator)
    {
        $this->invalidator = $invalidator;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Invalidates all cache');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $successful = $this->invalidator->bustAllCache();

        if ($successful) {
            $output->writeln('Cache invalidated successfully');

            return 0;
        }

        $output->writeln('There was an issue invalidating cache. Please see logs');

        return 1;
    }
}
