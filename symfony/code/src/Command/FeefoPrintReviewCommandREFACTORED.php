<?php

declare(strict_types=1);

namespace App\Command;

use App\Cache\Feefo\FeefoRequesterInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FeefoPrintReviewCommandREFACTORED extends Command
{
    /**
     * @var FeefoRequesterInterface
     */
    private $feefoRequester;

    protected static $defaultName = 'feefo:print-reviews';

    public function __construct(FeefoRequesterInterface $feefoRequester)
    {
        $this->feefoRequester = $feefoRequester;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Prints out the Feefo reviews for a product')
            ->addArgument('id', InputArgument::REQUIRED, 'Id of the product');
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id');
        if (!\ctype_digit($id) || 1 > (int) $id) {
            throw new \InvalidArgumentException('Please provide a valid, numerical id');
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $reviews = $this->feefoRequester->getReviewsByProductId((int) $input->getArgument('id'));

        $output->writeln(
            sprintf(
                'There are %d reviews and the average is %d%%',
                $reviews->getReviewCount(),
                $reviews->getAverage()
            )
        );

        return 0;
    }
}
