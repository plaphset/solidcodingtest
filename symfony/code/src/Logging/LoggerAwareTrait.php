<?php

declare(strict_types=1);

namespace App\Logging;

use Psr\Log\LoggerAwareTrait as BaseLoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

trait LoggerAwareTrait
{
    use BaseLoggerAwareTrait;

    public function getLogger(): LoggerInterface
    {
        if (null === $this->logger) {
            $this->logger = new NullLogger();
        }

        return $this->logger;
    }
}
