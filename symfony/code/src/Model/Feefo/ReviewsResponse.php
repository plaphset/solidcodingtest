<?php

declare(strict_types=1);

namespace App\Model\Feefo;

/**
 * A minimal model implementation, presuming these are the values that are needed
 * for reusability throughout the main Amara system.
 *
 * I would expect Amara to have full feefo response reusable models in the real world,
 * likely embedabble too for different levels of depth,
 * and regardless if they are unserialized from XML with 'DISABLE_TYPE_ENFORCEMENT'
 * or transformed from XML (precisely due to the format's limitations) - dedicated transforming being my choice here obviously
 */
class ReviewsResponse
{
    /**
     * @var int
     */
    private $productId;

    /**
     * @var int|null
     */
    private $reviewCount;

    /**
     * @var int|null
     */
    private $best;

    /**
     * @var int|null
     */
    private $worst;

    /**
     * @var int|null
     */
    private $average;

    public function __construct(int $productId)
    {
        $this->productId = $productId;
    }

    public function getProductId(): int
    {
        return $this->productId;
    }

    public function getReviewCount(): ?int
    {
        return $this->reviewCount;
    }

    public function setReviewCount(?int $reviewCount): ReviewsResponse
    {
        $this->reviewCount = $reviewCount;

        return $this;
    }

    public function getBest(): ?int
    {
        return $this->best;
    }

    public function setBest(?int $best): ReviewsResponse
    {
        $this->best = $best;

        return $this;
    }

    public function getWorst(): ?int
    {
        return $this->worst;
    }

    public function setWorst(?int $worst): ReviewsResponse
    {
        $this->worst = $worst;

        return $this;
    }

    public function getAverage(): ?int
    {
        return $this->average;
    }

    public function setAverage(?int $average): ReviewsResponse
    {
        $this->average = $average;

        return $this;
    }
}
