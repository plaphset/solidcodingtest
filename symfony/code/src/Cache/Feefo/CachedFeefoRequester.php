<?php

declare(strict_types=1);

namespace App\Cache\Feefo;

use App\Cache\TtlTrait;
use App\Model\Feefo\ReviewsResponse;
use App\ApiUtil\Feefo\FeefoRequester;
use Psr\Cache\CacheItemPoolInterface;

class CachedFeefoRequester implements FeefoRequesterInterface
{
    use TtlTrait;

    public const CACHE_PREFIX = 'CachedFeefoRequester';

    private $cache;
    private $service;

    public function __construct(CacheItemPoolInterface $adapter, FeefoRequester $service)
    {
        $this->cache = $adapter;
        $this->service = $service;
    }

    public function getReviewsByProductId(int $productId): ?ReviewsResponse
    {
        $key = $this->buildCacheKey($productId);

        $cachedItem = $this->cache->getItem($key);

        if (!$cachedItem->isHit()) {
            $reviews = $this->service->getReviewsByProductId($productId);
            $cachedItem->set($reviews);
            $cachedItem->expiresAfter($this->getTtl());
            $this->cache->save($cachedItem);
        }

        return $cachedItem->get();
    }

    private function buildCacheKey(int $productId): string
    {
        return md5(implode(':', [static::CACHE_PREFIX, $productId]));
    }
}
