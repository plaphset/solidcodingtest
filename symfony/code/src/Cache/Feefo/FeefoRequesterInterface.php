<?php

declare(strict_types=1);

namespace App\Cache\Feefo;

use App\Model\Feefo\ReviewsResponse;

interface FeefoRequesterInterface
{
    public function getReviewsByProductId(int $productId): ?ReviewsResponse;
}
