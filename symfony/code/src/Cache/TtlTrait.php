<?php

declare(strict_types=1);

namespace App\Cache;

trait TtlTrait
{
    protected $ttl = 3600;

    public function getTtl(): int
    {
        return $this->ttl;
    }

    public function setTtl(int $ttl): self
    {
        $this->ttl = $ttl;

        return $this;
    }
}
