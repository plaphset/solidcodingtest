<?php

declare(strict_types=1);

namespace App\Cache;

use App\Logging\LoggerAwareTrait;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Log\LoggerAwareInterface;
use Symfony\Component\Cache\Adapter\RedisAdapter;

class Invalidator implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var CacheItemPoolInterface|RedisAdapter
     */
    private $cache;

    /**
     * @param CacheItemPoolInterface|RedisAdapter $adapter
     */
    public function __construct(CacheItemPoolInterface $adapter)
    {
        $this->cache = $adapter;
    }

    public function bustAllCache(): bool
    {
        if (!$this->cache->clear()) {
            $this->getLogger()->error('Cache was not cleared: internal cache engine issue encountered');

            return false;
        }

        return true;
    }
}
