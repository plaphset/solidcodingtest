<?php

declare(strict_types=1);

namespace App\DataTransformer;

interface DataTransformerInterface
{
    /**
     * @param mixed $from
     * @param mixed $to
     *
     * @return mixed
     */
    public function transform($from, object $to = null);
}
