<?php

declare(strict_types=1);

namespace App\DataTransformer\Feefo;

use App\DataTransformer\DataTransformerInterface;
use App\Model\Feefo\ReviewsResponse;

class ReviewsResponseTransformer implements DataTransformerInterface
{
    public function transform($from, object $to = null)
    {
        $input = simplexml_load_string($from);
        if (!$input instanceof \SimpleXMLElement) {
            throw new \InvalidArgumentException('Provided data is not valid XML');
        }

        $id = $input->SUMMARY->VENDORREF;

        if (!isset($id) || 1 > ((int) $id)) {
            throw new \InvalidArgumentException('XML provided does not have expected format');
        }

        $model = new ReviewsResponse((int) $id);

        if (isset($input->SUMMARY->COUNT)) {
            $model->setReviewCount((int) $input->SUMMARY->COUNT);
        }

        if (isset($input->SUMMARY->BEST)) {
            $model->setBest((int) $input->SUMMARY->BEST);
        }

        if (isset($input->SUMMARY->WORST)) {
            $model->setWorst((int) $input->SUMMARY->WORST);
        }

        if (isset($input->SUMMARY->AVERAGE)) {
            $model->setAverage((int) $input->SUMMARY->AVERAGE);
        }

        return $model;
    }
}
