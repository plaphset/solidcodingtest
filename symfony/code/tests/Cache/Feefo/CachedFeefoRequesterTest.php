<?php

declare(strict_types=1);

namespace App\Tests\Cache\Feefo;

use App\ApiUtil\Feefo\FeefoRequester;
use App\Cache\Feefo\CachedFeefoRequester;
use App\Model\Feefo\ReviewsResponse;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Cache\Adapter\ArrayAdapter;

class CachedFeefoRequesterTest extends TestCase
{
    /**
     * @var FeefoRequester&MockObject
     */
    private $feefoRequester;

    /**
     * @var CachedFeefoRequester
     */
    private $cacheService;

    protected function setUp(): void
    {
        $this->feefoRequester = $this->createMock(FeefoRequester::class);
        $this->cacheService = new CachedFeefoRequester(new ArrayAdapter(), $this->feefoRequester);
    }

    /**
     * @test
     */
    public function saves_in_cache_and_then_is_hit(): void
    {
        $responseModel = (new ReviewsResponse(1000));
        $this->feefoRequester
            ->expects($this->once())
            ->method('getReviewsByProductId')
            ->with(1000)
            ->willReturn($responseModel);

        $result = $this->cacheService->getReviewsByProductId(1000);
        $this->assertEquals($responseModel, $result);

        $this->feefoRequester
            ->expects($this->never())
            ->method('getReviewsByProductId');

        $this->cacheService->getReviewsByProductId(1000);
    }
}
