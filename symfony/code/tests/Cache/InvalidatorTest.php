<?php

declare(strict_types=1);

namespace App\Tests\Cache;

use App\Cache\Invalidator;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Cache\CacheItemPoolInterface;

class InvalidatorTest extends TestCase
{
    /**
     * @var CacheItemPoolInterface&MockObject
     */
    private $adapter;

    /**
     * @var Invalidator
     */
    private $totalInvalidator;

    protected function setUp(): void
    {
        $this->adapter = $this->createMock(CacheItemPoolInterface::class);
        $this->totalInvalidator = new Invalidator($this->adapter);
    }

    /**
     * @test
     * @dataProvider cacheClearingProvider
     */
    public function clears_cache(bool $cacheCleared, bool $expectedResult): void
    {
        $this->adapter
            ->expects($this->once())
            ->method('clear')
            ->willReturn($cacheCleared);

        $actual = $this->totalInvalidator->bustAllCache();

        static::assertEquals($expectedResult, $actual);
    }

    public function cacheClearingProvider(): \Generator
    {
        yield [true, true];
        yield [false, false];
    }
}
