<?php

declare(strict_types=1);

namespace App\Tests\DataTransformer\Feefo;

use App\DataTransformer\Feefo\ReviewsResponseTransformer;
use App\Model\Feefo\ReviewsResponse;
use PHPUnit\Framework\TestCase;

class ReviewsResponseTransformerTest extends TestCase
{
    /**
     * @var ReviewsResponseTransformer
     */
    private $transformer;

    protected function setUp(): void
    {
        $this->transformer = new ReviewsResponseTransformer();
    }

    /**
     * @test
     */
    public function it_fully_transforms(): void
    {
        $feefoResponse = file_get_contents(__DIR__.'/data/full-and-ok-feefo-response.xml');
        $actualTransformed = $this->transformer->transform($feefoResponse);

        // please see note in 'ReviewsResponse' model class about a 'full' model
        $this->assertEquals(
            (new ReviewsResponse(42687))
                ->setAverage(100)
                ->setBest(100)
                ->setWorst(0)
                ->setReviewCount(4),
            $actualTransformed
        );
    }

    /**
     * @test
     */
    public function transforms_from_partial_data(): void
    {
        $feefoResponse = file_get_contents(__DIR__.'/data/partial-but-ok-feefo-response.xml');
        $actualTransformed = $this->transformer->transform($feefoResponse);

        $this->assertEquals((new ReviewsResponse(42687))->setAverage(100), $actualTransformed);
    }

    /**
     * @test
     */
    public function throws_invalid_exception(): void
    {
        $feefoResponse = file_get_contents(__DIR__.'/data/missing-crucial-data-response.xml');
        static::expectException(\InvalidArgumentException::class);
        static::expectExceptionMessage('XML provided does not have expected format');
        $this->transformer->transform($feefoResponse);

        // and case when data is not even an xml
        static::expectException(\InvalidArgumentException::class);
        static::expectExceptionMessage('Provided data is not valid XML');
        $this->transformer->transform('yippee-ki-yay');
    }
}
