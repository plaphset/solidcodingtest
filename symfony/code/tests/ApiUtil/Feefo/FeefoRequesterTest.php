<?php

declare(strict_types=1);

namespace App\Tests\ApiUtil\Feefo;

use App\ApiUtil\Feefo\FeefoRequester;
use App\DataTransformer\Feefo\ReviewsResponseTransformer;
use App\Model\Feefo\ReviewsResponse;
use GuzzleHttp\Client;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

class FeefoRequesterTest extends TestCase
{
    /**
     *  @var ReviewsResponseTransformer
     */
    private $transformer;

    /**
     * @var Client&MockObject
     */
    private $httpClient;

    /**
     * @var LoggerInterface&MockObject
     */
    private $logger;

    /**
     * @var FeefoRequester
     */
    private $requester;

    protected function setUp(): void
    {
        $this->transformer = $this->createMock(ReviewsResponseTransformer::class);
        $this->httpClient = $this->createMock(Client::class);
        $this->logger = $this->createMock(LoggerInterface::class);

        $this->requester = new FeefoRequester($this->transformer, $this->httpClient);
        $this->requester->setLogger($this->logger);
    }

    /**
     * @test
     */
    public function fetches_and_returns_transformed(): void
    {
        $body = $this->createConfiguredMock(StreamInterface::class, ['getContents' => 'theoretical XML']);
        $response = $this->createConfiguredMock(ResponseInterface::class, [
            'getStatusCode' => Response::HTTP_OK,
            'getBody' => $body,
            ]);
        $this->httpClient
            ->expects($this->once())
            ->method('request')
            ->with(
                'GET',
                FeefoRequester::FEEFO_XML_FEED_REVIEWS,
                ['query' => ['logon' => 'www.amara.co.uk', 'vendorref' => 13, 'limit' => 1]]
            )
            ->willReturn($response);

        $this->transformer
            ->expects($this->once())
            ->method('transform')
            ->with('theoretical XML')
            ->willReturn(new ReviewsResponse(13));

        $this->requester->getReviewsByProductId(13);
    }

    /**
     * @test
     */
    public function it_logs_request_failures(): void
    {
        $response = $this->createConfiguredMock(ResponseInterface::class, [
            'getStatusCode' => Response::HTTP_FORBIDDEN,
            'getBody' => 'supposed body',
        ]);
        $query = ['logon' => 'www.amara.co.uk', 'vendorref' => 13, 'limit' => 1];
        $this->httpClient
            ->expects($this->once())
            ->method('request')
            ->with(
                'GET',
                FeefoRequester::FEEFO_XML_FEED_REVIEWS,
                ['query' => $query]
            )
            ->willReturn($response);

        $this->transformer->expects($this->never())->method('transform');

        $this->logger
            ->expects($this->once())
            ->method('error')
            ->with(
                'Something went wrong fetching data from Feefo. Please see log context',
                [
                    'requestQuery' => $query,
                    'responseCode' => Response::HTTP_FORBIDDEN,
                    'responseBody' => 'supposed body',
                    'responseHeaders' => null,
                ]
            );

        $this->requester->getReviewsByProductId(13);
    }
}
